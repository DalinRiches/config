let g:vim_home = '~/.config/nvim/'

" Load plugins
exec 'source' g:vim_home.'plugin.vim'

" Load all vim configs
let configs = [
      \ 'config.vim',
      \ 'command.vim',
      \ 'autocmd.vim',
      \ 'mapping.vim',
      \ 'plugin/*.vim'
      \]

for files in configs
  for f in glob(g:vim_home.files, 1, 1)
    exec 'source' f
  endfor
endfor

" Set at the end to work around 'exrc'
set secure

" Gotta be first to be IMproved
set nocompatible
filetype off

" --- General Settings ---
set ruler
set number relativenumber
set showcmd
set incsearch
set nohlsearch
set nowrap
set hidden
set updatetime=300
set textwidth=0 wrapmargin=0
set clipboard=unnamedplus

syntax on

"--- Tab Settings -------------------------------------------------------------
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set autoindent

let g:python3_host_prog = "/usr/bin/python3"
let g:python_host_prog = "/usr/bin/python2"

"--- Folds -------------------------------------------------------------------
let c_no_comment_fold = 1
set foldnestmax=1
set foldmethod=indent

"--- COLORS -------------------------------------------------------------------
colorscheme jellybeans 

"Make Background Transparent
set fillchars+=vert:│
set foldcolumn=2
highlight Normal                guibg=NONE ctermbg=NONE
highlight NonText               guibg=NONE ctermbg=NONE
highlight SignColumn            guibg=NONE ctermbg=NONE
highlight LineNr                guibg=NONE ctermbg=NONE
highlight CursorLineNr          guibg=NONE ctermbg=NONE
highlight foldcolumn            guibg=NONE ctermbg=NONE
highlight Folded                guibg=NONE ctermbg=NONE
highlight VertSplit             guibg=NONE ctermbg=NONE ctermfg=NONE
highlight GitGutterAdd          guibg=NONE ctermbg=NONE ctermfg=green
highlight GitGutterChange       guibg=NONE ctermbg=NONE ctermfg=yellow
highlight GitGutterDelete       guibg=NONE ctermbg=NONE ctermfg=red
highlight GitGutterChangeDelete guibg=NONE ctermbg=NONE ctermfg=yellow
highlight ALEError              guibg=NONE ctermbg=NONE ctermfg=red
highlight ALEErrorSign          guibg=NONE ctermbg=NONE ctermfg=red
highlight ALEWarningSign        guibg=NONE ctermbg=NONE ctermfg=yellow
highlight ALEWarning            guibg=NONE ctermbg=NONE ctermfg=yellow


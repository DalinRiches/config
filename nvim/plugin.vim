call plug#begin('~/.vim/plugged')

Plug 'VundleVim/Vundle.vim'
Plug 'justinmk/vim-syntax-extra'

"--- Version Control Plugins
Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

"--- Languge Plugins ---
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'cespare/vim-toml'
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }

"--- Effeciency Enhancement Plugins ---
Plug 'ludovicchabant/vim-gutentags'
Plug 'townk/vim-autoclose'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'preservim/nerdcommenter'
Plug 'preservim/tagbar'
Plug 'kyazdani42/nvim-web-devicons' " for file icons
Plug 'vim-scripts/DoxygenToolkit.vim'

"--- Themeing Plugins ---
Plug 'sheerun/vim-polyglot'
Plug 'itchyny/lightline.vim'
Plug 'nanotech/jellybeans.vim'
Plug 'mhinz/vim-startify'

"--- IDE Plugins ---
Plug 'tpope/vim-vinegar'
Plug 'vimwiki/vimwiki'
Plug 'preservim/nerdtree'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'

" All of your Plugins must be added before the following line
call plug#end()

" --- itchyny/lightline.vim ---
" Always show statusbar
set laststatus=2
let g:lightline = {
   \ 'colorscheme': 'jellybeans',
   \ }

" Make the middle sections background transparent
let s:palette = g:lightline#colorscheme#{g:lightline.colorscheme}#palette
let s:palette.normal.middle = [ [ 'NONE', 'NONE', 'NONE', 'NONE' ] ]
let s:palette.inactive.middle = s:palette.normal.middle
let s:palette.tabline.middle = s:palette.normal.middle

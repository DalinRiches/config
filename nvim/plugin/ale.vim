" --- w0rp/ale ---
let g:ale_completion_enabled = 0
let g:ale_set_highlights = 0
let g:ale_sign_error = '▶'
let g:ale_sign_warning = '▷'
let g:ale_linters = {'c': ['clangd']}
let g:ale_linters.rust = ['cargo', 'rls']
let g:ale_rust_rls_toolchain = 'stable'
let g:rustfmt_autosave = 1

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=niv
endif

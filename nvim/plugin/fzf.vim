" -----------------------------------------------------------------------------
" junegunn/fzf.vim config 
" -----------------------------------------------------------------------------
let FZF_DEFAULT_COMMAND='ag --hidden -g ""'

" Setup the layout of fzf windows
"   - Open below
"   - 15 lines in height
" -------------------------------
let g:fzf_layout = {'down': '15'}

" -----------------------------------------------------------------------------
" Plugin commands 
" -----------------------------------------------------------------------------
command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, {'options': '--delimiter : --nth 4..'}, <bang>0)

" -----------------------------------------------------------------------------
" Plugin keybindings
" -----------------------------------------------------------------------------
nnoremap <silent> <Leader>f :Files<CR>
nnoremap <silent> <Leader>b :Buffers<CR>
nnoremap <silent> <Leader>s :Ag<CR>
nnoremap <silent> <Leader>F :Ag <c-r>=expand('<cword>')<CR><CR>


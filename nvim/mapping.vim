" -----------------------------------------------------------------------------
"  Global Mappings
" -----------------------------------------------------------------------------
set mouse=a

vmap <C-c> :call Copy()<CR>
nmap <silent> <Leader>d <Plug>(coc-definition)
nmap <silent> <Leader>i <Plug>(coc-implementation)
nmap <silent> <Leader>r <Plug>(coc-references)
nmap <silent> <Leader>v :NERDTreeToggle<CR>


" -----------------------------------------------------------------------------
"  Plugin Mappings
" -----------------------------------------------------------------------------
nnoremap <silent> <Leader>D :Dox<CR>
